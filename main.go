package main

import (
	"flag"
	"sync"

	permission "bitbucket.org/vettedio/ms-lookup/permissions"
	"bitbucket.org/vettedio/ms-lookup/service"
	"bitbucket.org/vettedio/utils/microservice"
)

var (
	wg         sync.WaitGroup
	configDir  = flag.String("conf.dir", "./conf", "Config directory for viper files")  // reads in the Config directory from the flags set in the startstop.sh
	configFile = flag.String("conf.file", "config", "Config file name (no extension)")  // reads in the Config file name from the flags set in the startstop.sh
	logFile    = flag.String("log.file", "./logs/ms-lookup", "log file (no extension)") // reads in the Log file name from the flags set in the startstop.sh
	version    = flag.String("service.version", "v0.0.1", "microservice version used for metrics and logging")
)

func main() {
	flag.Parse()
	wg.Add(1)

	_microservice := microservice.
		Create(*configFile, *configDir, logFile, *version).
		AddHandler("/getprovinces", service.GetProvinces, []microservice.Permission{permission.LookupQuery}, nil).
		AddHandler("/getentitytypes", service.GetEntityTypes, []microservice.Permission{permission.LookupQuery}, nil).
		Run(service.Run)

	defer _microservice.Shutdown()

	wg.Wait()
}
