FROM alpine:latest
ARG version=v0.0.0
RUN mkdir /app
RUN mkdir /app/conf
RUN mkdir /app/logs
WORKDIR /app/
COPY /conf /app/conf
ADD ms-lookup  /app/ms-lookup

CMD ["./ms-lookup"]
