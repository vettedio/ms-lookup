#!/bin/bash
service="ms-lookup"
repo="vetted"

version='v0.0.0'
debug=0

while getopts "dv:" opt; do
  case $opt in
    d)
      debug=1
      echo "Debug Mode"
      ;;
    v)
      version="${OPTARG}"
      ;;
  esac
done

if [[ $version == "v0.0.0" ]]; then
  echo "Please Specify Version Using -v Argument. Example: -v v0.0.1"
  if [[ debug -eq 1 ]]; then
     read -p "Press enter to continue"
     exit
  fi
fi

echo Microservice: $service
echo Version: $version
echo Building...

#version=`git describe --abbrev=0`

modified=`git status --short`
if [[ $modified != "" ]]; then
  echo "Project Modified. Please Commit Changes Before Deploying"
  if [[ debug -eq 1 ]]; then
     read -p "Press enter to continue"
  fi
  exit
fi

GOOS=linux GOARCH=amd64 go build

executable=`ls ${service}`
echo $executable
if [[ ($executable != $service) ]]; then
  echo "Build Failure."

  if [[ debug -eq 1 ]]; then
     read -p "Press enter to continue"
  fi

  exit
fi

commit=`git rev-parse --short HEAD`
echo "Tagging Commit: ${commit}"
git tag -a ${version} -m "deploying: ${version}"

echo Building Docker Image
docker build --build-arg version=${version} -t $service .
tag=`docker images ${service}:latest --format "{{.ID}}"`
echo DockerID: $tag
echo docker tag $tag ${repo}/${service}:${version}

docker tag $tag ${repo}/${service}:${version}
docker push ${repo}/${service}:${version}

echo "Build Successful"