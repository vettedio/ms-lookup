package permissions

import "bitbucket.org/vettedio/utils/microservice"

var LookupQuery = microservice.Permission{
	Permission: "lookupQuery",
	Description: "Allows the Querying of Lookup Tables",
}
