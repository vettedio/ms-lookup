package service

import (
	"net/http"

	"bitbucket.org/vettedio/utils/microservice"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Province struct {
	ProvinceId int    `gorm:"primary_key"`
	Name       string `gorm:"not null"`
}

type EntityType struct {
	EntityTypeId int    `gorm:"primary_key"`
	EntityType   string `gorm:"not null"`
}

type Gender struct {
	GenderId int    `gorm:"primary_key"`
	Gender   string `gorm:"not null"`
}

func Run(service *microservice.Microservice) {
	if service.DB != nil {

		service.DB.AutoMigrate(&Province{})
		service.DB.AutoMigrate(&EntityType{})

		provinces := []Province{
			{ProvinceId: 1, Name: "Limpopo"},
			{ProvinceId: 2, Name: "Gauteng"},
			{ProvinceId: 3, Name: "Mpumalanga"},
			{ProvinceId: 4, Name: "NorthWest"},
			{ProvinceId: 5, Name: "KwaZulu-Natal"},
			{ProvinceId: 6, Name: "FreeState"},
			{ProvinceId: 7, Name: "Northen Cape"},
			{ProvinceId: 8, Name: "Eastern Cape"},
			{ProvinceId: 9, Name: "Western Cape"}}

		entityTypes := []EntityType{
			{EntityTypeId: 1, EntityType: "Admin"},
			{EntityTypeId: 2, EntityType: "Company"},
			{EntityTypeId: 3, EntityType: "Service Provider"},
			{EntityTypeId: 4, EntityType: "Employee"}}

		genders := []Gender{
			{GenderId: 1, Gender: "Male"},
			{GenderId: 2, Gender: "Female"},
			{GenderId: 3, Gender: "Other"},
		}

		for _, province := range provinces {
			service.DB.Create(&province)
		}

		for _, entityType := range entityTypes {
			service.DB.Create(&entityType)
		}

		for _, gender := range genders {
			service.DB.Create(&gender)
		}
	}
}

func GetProvinces(service *microservice.Microservice, request microservice.Request) (response microservice.Response) {
	var provinces []Province
	result := service.DB.Find(&provinces)
	if result.Error != nil {
		var err = microservice.ErrResponse{Error: result.Error, Message: result.Error.Error(), ErrorCode: http.StatusInternalServerError}
		return microservice.Response{Error: &err}
	} else {
		return microservice.Response{Result: &provinces}
	}
}

func GetEntityTypes(service *microservice.Microservice, request microservice.Request) (response microservice.Response) {
	var entityTypes []EntityType
	result := service.DB.Find(&entityTypes)
	if result.Error != nil {
		var err = microservice.ErrResponse{Error: result.Error, Message: result.Error.Error(), ErrorCode: http.StatusInternalServerError}
		return microservice.Response{Error: &err}
	} else {
		return microservice.Response{Result: &entityTypes}
	}
}

func GetGenders(service *microservice.Microservice, request microservice.Request) (response microservice.Response) {
	var genders []Gender
	result := service.DB.Find(&genders)
	if result.Error != nil {
		var err = microservice.ErrResponse{Error: result.Error, Message: result.Error.Error(), ErrorCode: http.StatusInternalServerError}
		return microservice.Response{Error: &err}
	} else {
		return microservice.Response{Result: &genders}
	}
}
