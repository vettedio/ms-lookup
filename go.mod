module bitbucket.org/vettedio/ms-lookup

go 1.16

require (
	bitbucket.org/vettedio/utils v0.0.66
	github.com/jinzhu/gorm v1.9.16
	github.com/stretchr/testify v1.7.0 // indirect
)
